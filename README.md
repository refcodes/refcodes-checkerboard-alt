# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This repository provides artifacts for alternate implementations of the [`org.refcodes:refcodes-checkerboard`](https://bitbucket.org/refcodes/refcodes-checkerboard) framework.***

## How do I get set up? ##

Include the according dependencies of the artifacts you want to use in your project's `pom.xml`.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-checkerboard-alt/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.

---

The resources found at [refcodes-data-ext-chess/src/main/resources/org/refcodes/data/ext/chess](https://bitbucket.org/refcodes/refcodes-data-ext/src/master/refcodes-data-ext-chess/src/main/resources/org/refcodes/data/ext/chessmen/)  ("`chessmen`") are images converted from `SVG` to `FXML` to be directly usable by `JavaFX`. The original `SVG` files and therewith the therefrom derived `FXML` files are published under the [following licenses](https://bitbucket.org/refcodes/refcodes-checkerboard/src/master/src/main/resources/chessmen/LICENSING.md). Please make sure to read and regard them licensing terms and conditions when using them images in your own work.

---

The resources found at [refcodes-data-ext-checkers/src/main/resources/org/refcodes/data/ext/checkers](https://bitbucket.org/refcodes/refcodes-data-ext/src/master/refcodes-data-ext-checkers/src/main/resources/org/refcodes/data/ext/checkers/)  ("`checkers`") are images converted from `SVG` to `FXML` to be directly usable by `JavaFX`. The original `SVG` files and therewith the therefrom derived `FXML` files are published under the [following licenses](https://bitbucket.org/refcodes/refcodes-checkerboard/src/master/src/main/resources/checkers/LICENSING.md). Please make sure to read and regard them licensing terms and conditions when using them images in your own work.