module org.refcodes.checkerboard.alt.javafx {
	requires javafx.base;
	requires org.refcodes.mixin;
	requires transitive javafx.graphics;
	requires transitive org.refcodes.checkerboard;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.graphical;
	requires transitive org.refcodes.graphical.ext.javafx;
	requires transitive org.refcodes.observer;
	requires java.logging;

	exports org.refcodes.checkerboard.alt.javafx;
}
