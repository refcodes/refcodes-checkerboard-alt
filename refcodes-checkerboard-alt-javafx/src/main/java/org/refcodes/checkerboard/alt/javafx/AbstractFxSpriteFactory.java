package org.refcodes.checkerboard.alt.javafx;

import javafx.scene.Node;

/**
 * A factory for creating JavaFx "sprites".
 *
 * @param <S> the generic type for the player's status.
 * @param <B> the generic type of the builder to be returned upon invoking
 *        builder methods.
 */
public abstract class AbstractFxSpriteFactory<S, B extends AbstractFxSpriteFactory<S, B>> implements FxSpriteFactory<S> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private double _scaleFactor = 1;
	private double _opacity = 1;

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the scale factor.
	 *
	 * @return the scale factor
	 */
	public double getScaleFactor() {
		return _scaleFactor;
	}

	/**
	 * Sets the scale factor.
	 *
	 * @param aScaleFactor the new scale factor
	 */
	public void setScaleFactor( double aScaleFactor ) {
		_scaleFactor = aScaleFactor;
	}

	/**
	 * With scale factor.
	 *
	 * @param aScaleFactor the scale factor
	 * 
	 * @return the nf
	 */
	@SuppressWarnings("unchecked")
	public B withScaleFactor( double aScaleFactor ) {
		_scaleFactor = aScaleFactor;
		return (B) this;
	}

	/**
	 * Gets the opacity.
	 *
	 * @return the opacity
	 */
	public double getOpacity() {
		return _opacity;
	}

	/**
	 * Sets the opacity.
	 *
	 * @param aOpacity the new opacity
	 */
	public void setOpacity( double aOpacity ) {
		_opacity = aOpacity;
	}

	/**
	 * With opacity.
	 *
	 * @param aOpacity the opacity
	 * 
	 * @return the nf
	 */
	@SuppressWarnings("unchecked")
	public B withOpacity( double aOpacity ) {
		_opacity = aOpacity;
		return (B) this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the scale X.
	 *
	 * @param aSprite the sprite
	 * @param aCheckerboard the checkerboard
	 * 
	 * @return the scale X
	 */
	protected static double getScaleX( Node aSprite, FxCheckerboardViewer<?, ?> aCheckerboard ) {
		return aCheckerboard.getFieldWidth() / aSprite.getBoundsInLocal().getWidth();
	}

	/**
	 * Gets the scale Y.
	 *
	 * @param aSprite the sprite
	 * @param aCheckerboard the checkerboard
	 * 
	 * @return the scale Y
	 */
	protected static double getScaleY( Node aSprite, FxCheckerboardViewer<?, ?> aCheckerboard ) {
		return aCheckerboard.getFieldHeight() / aSprite.getBoundsInLocal().getHeight();
	}

	/**
	 * Inits the sprite.
	 *
	 * @param aSprite the sprite
	 * @param aCheckerboard the checkerboard
	 * @param <N> The type of the sprite to be used.
	 * 
	 * @return the node
	 */
	protected <N extends Node> N toInitNode( N aSprite, FxCheckerboardViewer<?, ?> aCheckerboard ) {
		final double theScaleX = getScaleX( aSprite, aCheckerboard ) - ( 1 - _scaleFactor );
		final double theScaleY = getScaleY( aSprite, aCheckerboard ) - ( 1 - _scaleFactor );
		final double theScale = theScaleX > theScaleY ? theScaleX : theScaleY;
		aSprite.setScaleX( theScale );
		aSprite.setScaleY( theScale );
		aSprite.setOpacity( getOpacity() );
		return aSprite;
	}

	/**
	 * Inits the sprite.
	 *
	 * @param aSprite the sprite
	 * @param aScale The scale between 0 and 1.
	 * @param aCheckerboard the checkerboard.
	 * @param <N> The type of the sprite to be used.
	 * 
	 * @return the node
	 */
	protected <N extends Node> N toInitNode( double aScale, N aSprite, FxCheckerboardViewer<?, ?> aCheckerboard ) {
		final double theScaleX = getScaleX( aSprite, aCheckerboard ) - ( 1 - _scaleFactor );
		final double theScaleY = getScaleY( aSprite, aCheckerboard ) - ( 1 - _scaleFactor );
		final double theScale = theScaleX > theScaleY ? theScaleX : theScaleY;
		aSprite.setScaleX( theScale * aScale );
		aSprite.setScaleY( theScale * aScale );
		aSprite.setOpacity( getOpacity() );
		return aSprite;
	}
}
