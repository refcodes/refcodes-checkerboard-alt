package org.refcodes.checkerboard.alt.javafx;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.checkerboard.ChangePlayerPositionEvent;
import org.refcodes.checkerboard.Checkerboard;
import org.refcodes.checkerboard.CheckerboardEvent;
import org.refcodes.checkerboard.CheckerboardViewer;
import org.refcodes.checkerboard.GraphicalCheckerboardViewer;
import org.refcodes.checkerboard.GridDimensionChangedEvent;
import org.refcodes.checkerboard.GridModeChangedEvent;
import org.refcodes.checkerboard.GridPositionClickedEvent;
import org.refcodes.checkerboard.Player;
import org.refcodes.checkerboard.PlayerAddedEvent;
import org.refcodes.checkerboard.PlayerDraggabilityChangedEvent;
import org.refcodes.checkerboard.PlayerEvent;
import org.refcodes.checkerboard.PlayerPositionChangedEvent;
import org.refcodes.checkerboard.PlayerRemovedEvent;
import org.refcodes.checkerboard.PlayerStateChangedEvent;
import org.refcodes.checkerboard.PlayerVisibilityChangedEvent;
import org.refcodes.checkerboard.ViewportDimensionChangedEvent;
import org.refcodes.checkerboard.ViewportOffsetChangedEvent;
import org.refcodes.exception.Trap;
import org.refcodes.exception.VetoException;
import org.refcodes.exception.VetoException.VetoRuntimeException;
import org.refcodes.graphical.Dimension;
import org.refcodes.graphical.FieldDimension;
import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.MoveMode;
import org.refcodes.graphical.Offset;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.ScaleMode;
import org.refcodes.graphical.ViewportDimension;
import org.refcodes.graphical.ViewportDimensionPropertyBuilder;
import org.refcodes.graphical.ViewportOffset;
import org.refcodes.graphical.ext.javafx.AbstractFxGridViewportPane;
import org.refcodes.mixin.Disposable;
import org.refcodes.observer.SubscribeEvent;
import org.refcodes.observer.UnsubscribeEvent;

import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 * The class {@link FxCheckerboardViewer} uses the
 * {@link AbstractFxGridViewportPane} to implement a {@link CheckerboardViewer}.
 *
 * @param <P> The type representing a {@link Player}
 * @param <S> The type which's instances represent a {@link Player} state.
 */
public class FxCheckerboardViewer<P extends Player<P, S>, S> extends AbstractFxGridViewportPane<FxCheckerboardViewer<P, S>> implements GraphicalCheckerboardViewer<P, S, Node, FxSpriteFactory<S>, FxBackgroundFactory, FxCheckerboardViewer<P, S>> { // ], CheckerboardObserver<P, S> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	static final Logger LOGGER = Logger.getLogger( FxCheckerboardViewer.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int DEFAULT_MOVE_PLAYER_DURATION_MILLIS = 150;
	private static final int DEFAULT_CHANGE_PLAYER_STATE_DURATION_MILLIS = 100;
	private static final int DEFAULT_CHANGE_PLAYER_VISIBILITY_DURATION_MILLIS = 250;
	private static final int DEFAULT_REMOVE_PLAYER_DURATION_MILLIS = 250;
	private static final int DEFAULT_ADD_PLAYER_DURATION_MILLIS = 750;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	double _bordersH = Double.NaN;
	double _bordersV = Double.NaN;
	private Checkerboard<P, S> _checkerboard;
	private int _addPlayerDurationMillis = DEFAULT_ADD_PLAYER_DURATION_MILLIS;
	private int _changePlayerStateDurationMillis = DEFAULT_CHANGE_PLAYER_STATE_DURATION_MILLIS;
	private int _movePlayerDurationMillis = DEFAULT_MOVE_PLAYER_DURATION_MILLIS;
	private int _changePlayerVisibilityDurationMillis = DEFAULT_CHANGE_PLAYER_VISIBILITY_DURATION_MILLIS;
	private int _removePlayerDurationMillis = DEFAULT_REMOVE_PLAYER_DURATION_MILLIS;
	private final Map<P, ClickPlayerEventHandler> _playerToClickEventHandler = new HashMap<>();
	private final Map<P, DragPlayerEventHandler> _playerToDragEventHandler = new HashMap<>();
	protected Map<P, Node> _playerToSprite = new HashMap<>();
	double _windowDecorationH = Double.NaN;
	double _windowDecorationV = Double.NaN;
	private final ViewportDimensionPropertyBuilder _minViewportDimension = new ViewportDimensionPropertyBuilder( -1, -1 );
	private ScaleMode _scaleMode = ScaleMode.GRID;
	private FxSpriteFactory<S> _spriteFactory = null;
	protected Node _backgroundNode = null;
	protected Group _checkers = new Group();
	protected FxBackgroundFactory _backgroundFactory = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link FxCheckerboardViewer} instance. ATTENTION: As
	 * no {@link FxSpriteFactory} is provided to this constructor, no sprites
	 * can be fabricated when players are added until the
	 * {@link #setSpriteFactory(org.refcodes.checkerboard.SpriteFactory)} has
	 * been set!
	 *
	 * @param aCheckerboard the {@link Checkerboard} to be viewed.
	 */
	public FxCheckerboardViewer( Checkerboard<P, S> aCheckerboard ) {
		_checkerboard = aCheckerboard;
		aCheckerboard.subscribeObserver( this );
		onMouseClicked( ( x, y ) -> onGridPositionClickedEvent( new GridPositionClickedEvent<P, S>( x, y, aCheckerboard ) ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_checkerboard.destroy();
	}

	/**
	 * Gets the adds the player duration in milliseconds.
	 *
	 * @return the adds the player duration in milliseconds
	 */
	public int getAddPlayerDurationMillis() {
		return _addPlayerDurationMillis;
	}

	/**
	 * Sets the adds the player duration in milliseconds.
	 *
	 * @param aAddPlayerDurationMillis the new adds the player duration in
	 *        milliseconds
	 */
	public void setAddPlayerDurationMillis( int aAddPlayerDurationMillis ) {
		_addPlayerDurationMillis = aAddPlayerDurationMillis;
	}

	/**
	 * With add player duration in millis.
	 *
	 * @param aAddPlayerDurationMillis the add player duration in millis
	 * 
	 * @return the {@link FxCheckerboardViewer} as of the builder pattern.
	 */
	public FxCheckerboardViewer<P, S> withAddPlayerDurationMillis( int aAddPlayerDurationMillis ) {
		setAddPlayerDurationMillis( aAddPlayerDurationMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxBackgroundFactory getBackgroundFactory() {
		return _backgroundFactory;
	}

	/**
	 * Gets the change player state in millis.
	 *
	 * @return the change player state in millis
	 */
	public int getChangePlayerStateMillis() {
		return _changePlayerStateDurationMillis;
	}

	/**
	 * Sets the change player state in millis.
	 *
	 * @param aChangePlayerStateMillis the new change player state in millis
	 */
	public void setChangePlayerStateMillis( int aChangePlayerStateMillis ) {
		_changePlayerStateDurationMillis = aChangePlayerStateMillis;
	}

	/**
	 * With change player state in millis.
	 *
	 * @param aChangePlayerStateMillis the change player state in millis
	 * 
	 * @return the {@link FxCheckerboardViewer} as of the builder pattern.
	 */
	public FxCheckerboardViewer<P, S> withChangePlayerStateMillis( int aChangePlayerStateMillis ) {
		setChangePlayerStateMillis( aChangePlayerStateMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getContainerHeight() {
		return getFieldHeight() * getViewportHeight() + ( getFieldGap() * ( getViewportHeight() - 1 ) ) + ( ( _checkerboard.getGridMode() == GridMode.CLOSED ) ? ( getFieldGap() * 2 ) : 0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getContainerWidth() {
		return getFieldWidth() * getViewportWidth() + ( getFieldGap() * ( getViewportWidth() - 1 ) ) + ( ( _checkerboard.getGridMode() == GridMode.CLOSED ) ? ( getFieldGap() * 2 ) : 0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridHeight() {
		return _checkerboard.getGridHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GridMode getGridMode() {
		return _checkerboard.getGridMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridWidth() {
		return _checkerboard.getGridWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ViewportDimension getMinViewportDimension() {
		return _minViewportDimension;
	}

	/**
	 * Gets the move player duration in milliseconds.
	 *
	 * @return the move player duration in milliseconds
	 */
	public int getMovePlayerDurationMillis() {
		return _movePlayerDurationMillis;
	}

	/**
	 * Sets the move player duration in milliseconds.
	 *
	 * @param aMovePlayerDurationMillis the new move player duration in
	 *        milliseconds
	 */
	public void setMovePlayerDurationMillis( int aMovePlayerDurationMillis ) {
		_movePlayerDurationMillis = aMovePlayerDurationMillis;
	}

	/**
	 * With move player duration in milliseconds.
	 *
	 * @param aMovePlayerDurationMillis the move player duration in milliseconds
	 * 
	 * @return the {@link FxCheckerboardViewer} as of the builder pattern.
	 */
	public FxCheckerboardViewer<P, S> withMovePlayerDurationMillis( int aMovePlayerDurationMillis ) {
		setMovePlayerDurationMillis( aMovePlayerDurationMillis );
		return this;
	}

	/**
	 * Gets the change player visibility duration in milliseconds.
	 *
	 * @return the change player visibility duration in milliseconds
	 */
	public int getChangePlayerVisibilityDurationMillis() {
		return _changePlayerVisibilityDurationMillis;
	}

	/**
	 * Sets the change player visibility duration in milliseconds.
	 *
	 * @param aChangePlayerVisibilityDurationMillis the new change player
	 *        visibility duration in milliseconds
	 */
	public void setChangePlayerVisibilityDurationMillis( int aChangePlayerVisibilityDurationMillis ) {
		_changePlayerVisibilityDurationMillis = aChangePlayerVisibilityDurationMillis;
	}

	/**
	 * With change player visibility duration in milliseconds.
	 *
	 * @param aChangePlayerVisibilityDurationMillis the change player visibility
	 *        duration in milliseconds
	 * 
	 * @return the {@link FxCheckerboardViewer} as of the builder pattern.
	 */
	public FxCheckerboardViewer<P, S> withChangePlayerVisibilityDurationMillis( int aChangePlayerVisibilityDurationMillis ) {
		setChangePlayerVisibilityDurationMillis( aChangePlayerVisibilityDurationMillis );
		return this;
	}

	/**
	 * Gets the remove the player duration in milliseconds.
	 *
	 * @return the player remove duration in milliseconds
	 */
	public int getRemovePlayerDurationMillis() {
		return _removePlayerDurationMillis;
	}

	/**
	 * Sets the removes the player duration in millis.
	 *
	 * @param aRemovePlayerDurationMillis the new removes the player duration in
	 *        millis
	 */
	public void setRemovePlayerDurationMillis( int aRemovePlayerDurationMillis ) {
		_removePlayerDurationMillis = aRemovePlayerDurationMillis;
	}

	/**
	 * With remove player duration in millis.
	 *
	 * @param aRemovePlayerDurationMillis the remove player duration in millis
	 * 
	 * @return the {@link FxCheckerboardViewer} as of the builder pattern.
	 */
	public FxCheckerboardViewer<P, S> withRemovePlayerDurationMillis( int aRemovePlayerDurationMillis ) {
		setRemovePlayerDurationMillis( aRemovePlayerDurationMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ScaleMode getScaleMode() {
		return _scaleMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxSpriteFactory<S> getSpriteFactory() {
		return _spriteFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void initialize() {
		// Add players |-->
		for ( P ePlayer : _checkerboard.getPlayers() ) {
			if ( !_playerToSprite.containsKey( ePlayer ) ) {
				addPlayer( ePlayer, getAddPlayerDurationMillis() );
			}
		}
		// Add players <--|
		if ( Platform.isFxApplicationThread() ) {
			fxInitialize();
		}
		else {
			Platform.runLater( () -> fxInitialize() );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onChangePlayerPositionEvent( ChangePlayerPositionEvent<P> aEvent, Checkerboard<P, S> aCheckerboard ) throws VetoException {
		LOGGER.log( Level.FINE, aEvent.toString() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCheckerboardEvent( CheckerboardEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPlayerDraggabilityChangedEvent( PlayerDraggabilityChangedEvent<P> aEvent, Checkerboard<P, S> aCheckerboard ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		fxPlayerDraggability( aEvent.getSource() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onGridDimensionChangedEvent( GridDimensionChangedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onGridModeChangedEvent( GridModeChangedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		setGridMode( aEvent.getGridMode() );
		initMinStageWidth();
		initMinStageHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPlayerAddedEvent( PlayerAddedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		addPlayer( aEvent.getPlayer(), _addPlayerDurationMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPlayerEvent( PlayerEvent<P> aEvent, Checkerboard<P, S> aCheckerboard ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPlayerRemovedEvent( PlayerRemovedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		fxRemovePlayer( aEvent.getPlayer(), _removePlayerDurationMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPlayerPositionChangedEvent( PlayerPositionChangedEvent<P> aEvent, Checkerboard<P, S> aCheckerboard ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		fxMovePlayer( aEvent.getSource(), _movePlayerDurationMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPlayerStateChangedEvent( PlayerStateChangedEvent<P, S> aEvent, Checkerboard<P, S> aCheckerboard ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		final Node theSprite = createSprite( aEvent.getSource().getStatus() );
		final Node prevSprite = _playerToSprite.get( aEvent.getSource() );
		if ( theSprite != prevSprite ) {
			fxRemovePlayer( aEvent.getSource(), _changePlayerStateDurationMillis / 2 );
			addPlayer( aEvent.getSource(), _changePlayerStateDurationMillis / 2 );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSubscribe( SubscribeEvent<Checkerboard<P, S>> aSubscribeEvent ) {
		_checkerboard = aSubscribeEvent.getSource();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onUnsubscribe( UnsubscribeEvent<Checkerboard<P, S>> aUnsubscribeEvent ) {
		if ( _checkerboard == aUnsubscribeEvent.getSource() ) {
			_checkerboard = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onViewportDimensionChangedEvent( ViewportDimensionChangedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onGridPositionClickedEvent( GridPositionClickedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onViewportOffsetChangedEvent( ViewportOffsetChangedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPlayerVisibilityChangedEvent( PlayerVisibilityChangedEvent<P> aEvent, Checkerboard<P, S> aCheckerboard ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		fxChangePlayerVisibility( aEvent.getSource(), _changePlayerVisibilityDurationMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBackgroundFactory( FxBackgroundFactory aBackgroundFactory ) {
		_backgroundFactory = aBackgroundFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( Dimension aDimension ) {
		setFieldDimension( aDimension.getWidth(), aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( FieldDimension aField ) {
		setFieldDimension( aField.getFieldWidth(), aField.getFieldHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMinViewportDimension( Dimension aDimension ) {
		_minViewportDimension.setViewportDimension( aDimension );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMinViewportDimension( int aWidth, int aHeight ) {
		_minViewportDimension.setViewportDimension( aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMinViewportDimension( ViewportDimension aDimension ) {
		_minViewportDimension.setViewportDimension( aDimension );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setScaleMode( ScaleMode aMode ) {
		_scaleMode = aMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSpriteFactory( FxSpriteFactory<S> aSpriteFactory ) {
		_spriteFactory = aSpriteFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( Dimension aDimension ) {
		setViewportDimension( aDimension.getWidth(), aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( int aPosX, int aPosY ) {
		if ( aPosX != getViewportOffsetX() || aPosY != getViewportOffsetY() ) {
			final ViewportOffsetChangedEvent<P, S> theEvent = new ViewportOffsetChangedEvent<>( aPosX, aPosY, getViewportOffsetX(), getViewportOffsetY(), this );
			super.setViewportOffset( aPosX, aPosY );
			onViewportOffsetChangedEvent( theEvent );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void show() {
		setVisible( true );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void hide() {
		setVisible( false );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toTotalHeight() {
		int theTotalHeight = getFieldHeight() * getViewportHeight() + ( getFieldGap() * ( getViewportHeight() - 1 ) );
		if ( getGridMode() == GridMode.CLOSED ) {
			theTotalHeight += ( getFieldGap() * 2 );
		}
		return theTotalHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toTotalWidth() {
		int theTotalWidth = getFieldWidth() * getViewportWidth() + ( getFieldGap() * ( getViewportWidth() - 1 ) );
		if ( getGridMode() == GridMode.CLOSED ) {
			theTotalWidth += ( getFieldGap() * 2 );
		}
		return theTotalWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withBackgroundFactory( FxBackgroundFactory aBackgroundFactory ) {
		setBackgroundFactory( aBackgroundFactory );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withDragOpacity( double aOpacity ) {
		setDragOpacity( aOpacity );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withFieldDimension( Dimension aDimension ) {
		setFieldDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withFieldDimension( FieldDimension aField ) {
		setFieldDimension( aField );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withFieldDimension( int aFieldWidth, int aFieldHeight ) {
		setFieldDimension( aFieldWidth, aFieldHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withFieldDimension( int aFieldWidth, int aFieldHeight, int aGap ) {
		setFieldDimension( aFieldWidth, aFieldHeight, aGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withFieldGap( int aFieldGap ) {
		setFieldGap( aFieldGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withFieldHeight( int aHeight ) {
		setFieldHeight( aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withFieldWidth( int aWidth ) {
		setFieldWidth( aWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withGridMode( GridMode aGridMode ) {
		setGridMode( aGridMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withHide() {
		hide();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withInitialize() {
		initialize();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withMinViewportDimension( Dimension aDimension ) {
		setMinViewportDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withMinViewportDimension( int aWidth, int aHeight ) {
		setMinViewportDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withMinViewportDimension( ViewportDimension aDimension ) {
		setMinViewportDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withMoveMode( MoveMode aMode ) {
		setMoveMode( aMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withScaleMode( ScaleMode aMode ) {
		setScaleMode( aMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withShow() {
		show();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withSpriteFactory( FxSpriteFactory<S> aSpriteFactory ) {
		setSpriteFactory( aSpriteFactory );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportDimension( Dimension aDimension ) {
		setViewportDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportDimension( int aWidth, int aHeight ) {
		setViewportDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportDimension( ViewportDimension aGridDimension ) {
		setViewportDimension( aGridDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportHeight( int aGridHeight ) {
		setViewportHeight( aGridHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportOffset( int aPosX, int aPosY ) {
		setViewportOffset( aPosX, aPosY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportOffset( Offset aOffset ) {
		setViewportOffset( aOffset );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportOffset( Position aOffset ) {
		setViewportOffset( aOffset );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportOffset( ViewportOffset aOffset ) {
		setViewportOffset( aOffset );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportOffsetX( int aPosX ) {
		setViewportOffsetX( aPosX );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportOffsetY( int aPosY ) {
		setViewportOffsetY( aPosY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withViewportWidth( int aGridWidth ) {
		setViewportWidth( aGridWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withMoveViewportDurationMillis( int aMoveViewportDurationMillis ) {
		setMoveViewportDurationMillis( aMoveViewportDurationMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxCheckerboardViewer<P, S> withDragViewportDurationMillis( int aDragViewportDurationMillis ) {
		setDragViewportDurationMillis( aDragViewportDurationMillis );
		return this;
	}

	/**
	 * TODO 2023-12-06 Find out why we need to reconcile here.
	 * 
	 * TODO 2023-12-06 Determine if here is the right place reconcile.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void reconcile() {
		final Iterator<Node> e = _checkers.getChildren().iterator();
		Node eChecker;
		while ( e.hasNext() ) {
			eChecker = e.next();
			if ( eChecker.getUserData() != null ) {
				out: {
					synchronized ( _playerToSprite ) {
						for ( var eSprite : _playerToSprite.values() ) {
							if ( eChecker.getUserData() != null && eSprite.getUserData() != null && eChecker.getUserData().equals( eSprite.getUserData() ) ) {
								break out;
							}
						}
					}
					e.remove();
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [bordersH=" + _bordersH + ", bordersV=" + _bordersV + ", checkerboard=" + _checkerboard + ", containerHeight=" + getContainerHeight() + ", containerWidth=" + getContainerWidth() + "]";
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hide players.
	 *
	 * @param aDurationMillis the duration in milliseconds
	 */
	protected void hidePlayers( int aDurationMillis ) {
		for ( P ePlayer : _checkerboard.getPlayers() ) {
			fxHidePlayer( ePlayer, aDurationMillis );
		}
	}

	/**
	 * Reset players.
	 *
	 * @param aDurationMillis the duration in milliseconds
	 */
	protected void resetPlayers( int aDurationMillis ) {
		for ( P ePlayer : _checkerboard.getPlayers() ) {
			fxResetPlayer( ePlayer, aDurationMillis );
		}
	}

	/**
	 * Scale players.
	 *
	 * @param aFieldDimension the field dimension
	 * @param aPrecedingFieldDimension the preceding field dimension
	 */
	protected void scalePlayers( FieldDimension aFieldDimension, FieldDimension aPrecedingFieldDimension ) {
		for ( P ePlayer : _checkerboard.getPlayers() ) {
			fxScalePlayer( ePlayer, aFieldDimension );
		}
	}

	int toScaledFieldDimension( double aNewWindowDim, int aViewportDim, int aFieldDim, int aFieldGap, double aBordersDim ) {
		if ( !Double.isNaN( aBordersDim ) && aNewWindowDim != 1.0 ) {
			double theCheckerboardDim = aNewWindowDim - aBordersDim;
			// Just consider top (left) gap as we drag:
			theCheckerboardDim -= aFieldGap;
			final int theFieldDim = (int) Math.round( ( theCheckerboardDim - ( ( aViewportDim - 1 ) * aFieldGap ) ) / aViewportDim );
			if ( theFieldDim != aFieldDim ) {
				try {
					return theFieldDim;
				}
				catch ( VetoRuntimeException veto ) { /* Do nothing */ }
			}
		}
		return -1;
	}

	int toScaledViewportDimension( double aNewWindowDim, int aViewportDim, int aFieldDim, int aFieldGap, double aBordersDim ) {
		if ( !Double.isNaN( aBordersDim ) && aNewWindowDim != 1.0 ) {
			double theCheckerboardDim = aNewWindowDim - aBordersDim;
			// Just consider top (left) gap as we drag |-->
			theCheckerboardDim -= aFieldGap;
			// Just consider top (left) gap as we drag <--|
			final int theViewportDim = (int) Math.round( theCheckerboardDim / ( aFieldDim + aFieldGap ) );
			if ( theViewportDim != aViewportDim ) {
				try {
					return theViewportDim;
				}
				catch ( VetoRuntimeException veto ) { /* Do nothing */ }
			}
		}
		return -1;
	}

	void initBordersH( double aWindowWidth ) {
		if ( Double.isNaN( _bordersH ) ) {
			double theBoardersH = aWindowWidth - ( ( getViewportWidth() - 1 ) * getFieldGap() );
			if ( _checkerboard.getGridMode() == GridMode.CLOSED ) {
				theBoardersH -= getFieldGap() * 2;
			}
			theBoardersH -= ( getViewportWidth() * getFieldWidth() );
			// -----------------------------------------------------------------
			// Adjust one (phantom) gap for accurate calculation:
			// -----------------------------------------------------------------
			if ( _checkerboard.getGridMode() == GridMode.CLOSED ) {
				theBoardersH += getFieldGap();
			}
			else {
				theBoardersH -= getFieldGap();
			}
			// -----------------------------------------------------------------
			LOGGER.log( Level.FINE, "Horizontal (left and right) [phantom] borders from width <" + aWindowWidth + "> := " + theBoardersH );
			// ???: _windowDecorationH = _stage.getWidth() - _scene.getWidth();
			// ???: _bordersH = 0;
			_bordersH = theBoardersH;
		}
	}

	void initBordersV( double aWindowHeight ) {
		if ( Double.isNaN( _bordersV ) ) {
			double theBoardersV = aWindowHeight - ( ( getViewportHeight() - 1 ) * getFieldGap() );
			if ( _checkerboard.getGridMode() == GridMode.CLOSED ) {
				theBoardersV -= getFieldGap() * 2;
			}
			theBoardersV -= ( getViewportHeight() * getFieldHeight() );
			// -----------------------------------------------------------------
			// Adjust one (phantom) gap for accurate calculation:
			// -----------------------------------------------------------------
			if ( _checkerboard.getGridMode() == GridMode.CLOSED ) {
				theBoardersV += getFieldGap();
			}
			else {
				theBoardersV -= getFieldGap();
			}
			// -----------------------------------------------------------------
			LOGGER.log( Level.FINE, "Vertical (top and bottom) [phantom] borders from height <" + aWindowHeight + "> := " + theBoardersV );
			// ???: _windowDecorationV = _stage.getHeight() - _scene.getHeight();
			// ???: _bordersV = 0;
			_bordersV = theBoardersV;
		}
	}

	void initMinStageHeight() {
		if ( Double.isNaN( _bordersV ) ) {
			initBordersV( getHeight() );
		}
		else {
			int theHeight = (int) _bordersV;
			theHeight += ( getViewportHeight() * getFieldHeight() + getViewportHeight() * getFieldGap() );
			setMinHeight( theHeight + _windowDecorationV );
		}
	}

	void initMinStageWidth() {
		if ( Double.isNaN( _bordersH ) ) {
			initBordersH( getWidth() );
		}
		else {
			int theWidth = (int) _bordersH;
			theWidth += ( getViewportWidth() * getFieldWidth() + getViewportWidth() * getFieldGap() );
			setMinWidth( theWidth + _windowDecorationH );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void addPlayer( P aPlayer, int aDurationMillis ) {
		final Node theSprite = createSprite( aPlayer.getStatus() );
		putSprite( aPlayer, theSprite );
		// Mouse events |-->
		if ( aPlayer.isDraggable() ) {
			final DragPlayerEventHandler theDragEventHandler = new DragPlayerEventHandler( aPlayer, theSprite );
			_playerToDragEventHandler.put( aPlayer, theDragEventHandler );
		}
		final ClickPlayerEventHandler theClickEventHandler = new ClickPlayerEventHandler( aPlayer, theSprite );
		_playerToClickEventHandler.put( aPlayer, theClickEventHandler );
		// Mouse events <--|

		final Point2D thePoint = theSprite.localToParent( 0, 0 );
		theSprite.setLayoutX( -( thePoint.getX() ) );
		theSprite.setLayoutY( -( thePoint.getY() ) );
		theSprite.setTranslateX( toPixelPositionX( aPlayer ) );
		theSprite.setTranslateY( toPixelPositionY( aPlayer ) );

		final Runnable theRunner = () -> {
			if ( isViewport( aPlayer ) ) {
				_checkers.getChildren().add( theSprite );
				if ( aPlayer.isVisible() ) {
					if ( aDurationMillis > 0 ) {
						theSprite.setOpacity( 0 );
						final FadeTransition theTransition = new FadeTransition( Duration.millis( aDurationMillis ), theSprite );
						theTransition.setFromValue( 0 );
						theTransition.setToValue( 1 );
						theTransition.setCycleCount( 1 );
						theTransition.setAutoReverse( false );
						theTransition.play();
					}
					else {
						theSprite.setOpacity( 1 );
						theSprite.setVisible( true );
					}
				}
			}
			else {
				theSprite.setVisible( true );
				_checkers.getChildren().add( theSprite );
			}
		};
		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	private void fxHidePlayer( P aPlayer, int aDurationMillis ) {
		final Node theSprite = getSprite( aPlayer );
		if ( theSprite != null ) {
			if ( aDurationMillis == 0 ) {
				theSprite.setOpacity( 0 );
			}
			else if ( theSprite.getOpacity() != 0 ) {
				final Runnable theRunner = () -> { final FadeTransition theTransition = new FadeTransition( Duration.millis( aDurationMillis ), theSprite ); theTransition.setFromValue( theSprite.getOpacity() ); theTransition.setToValue( 0 ); theTransition.setCycleCount( 1 ); theTransition.setAutoReverse( false ); theTransition.play(); };
				if ( Platform.isFxApplicationThread() ) {
					theRunner.run();
				}
				else {
					Platform.runLater( theRunner );
				}
			}
		}
	}

	private void fxInitialize() {
		LOGGER.log( Level.FINE, "Initializing ..." );
		if ( getViewportWidth() == -1 ) {
			setViewportWidth( getGridWidth() );
		}
		if ( getViewportHeight() == -1 ) {
			setViewportHeight( getGridHeight() );
		}
		if ( getViewportWidth() == -1 || getViewportHeight() == -1 ) {
			throw new IllegalStateException( "The viewport dimension for the checkerboard must be set!" );
		}
		if ( _backgroundFactory != null ) {
			_backgroundNode = _backgroundFactory.create( this );
			_checkers.getChildren().add( 0, _backgroundNode );
		}
		switch ( getScaleMode() ) {
		case NONE -> {
			final StackPane thePane = new StackPane();
			thePane.getChildren().add( _checkers );
			StackPane.setAlignment( _checkers, Pos.CENTER );
			setRoot( thePane );
		}
		case GRID, FIELDS -> {
			setRoot( _checkers );
			setViewportOffset( this );
			setFieldDimension( this );
		}
		}
	}

	private void fxMovePlayer( P aPlayer, int aDurationMillis ) {
		final Node theSprite = getSprite( aPlayer );
		final Runnable theRunner = () -> {
			if ( _checkers.getChildren().remove( theSprite ) ) {
				_checkers.getChildren().add( theSprite );
				if ( getMoveMode() == MoveMode.SMOOTH && aDurationMillis != 0 ) {
					final TranslateTransition theTransition = new TranslateTransition( Duration.millis( aDurationMillis ), theSprite );
					theTransition.setByX( toPixelPositionX( aPlayer ) - theSprite.getTranslateX() );
					theTransition.setByY( toPixelPositionY( aPlayer ) - theSprite.getTranslateY() );
					theTransition.setCycleCount( 1 );
					theTransition.setAutoReverse( false );
					theTransition.play();
				}
				else {
					theSprite.setTranslateX( toPixelPositionX( aPlayer ) );
					theSprite.setTranslateY( toPixelPositionY( aPlayer ) );
				}
			}
		};
		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	private void fxPlayerDraggability( P aPlayer ) {
		final Node theSprite = getSprite( aPlayer );
		if ( theSprite != null ) {
			if ( aPlayer.isDraggable() && !_playerToDragEventHandler.containsKey( aPlayer ) ) {
				final DragPlayerEventHandler theDragEventHandler = new DragPlayerEventHandler( aPlayer, theSprite );
				_playerToDragEventHandler.put( aPlayer, theDragEventHandler );
			}
			else {
				final DragPlayerEventHandler theDragEventHandler = _playerToDragEventHandler.remove( aPlayer );
				if ( theDragEventHandler != null ) {
					theDragEventHandler.dispose();
				}
			}
		}
		else {
			throw new IllegalStateException( "The player <" + aPlayer + "> is unknwon by this checkerboard." );
		}
	}

	private void fxChangePlayerVisibility( P aPlayer, int aDurationMillis ) {
		final Node theSprite = getSprite( aPlayer );
		if ( theSprite != null ) {
			final FadeTransition theTransition = new FadeTransition( Duration.millis( aDurationMillis ), theSprite );
			theTransition.setFromValue( aPlayer.isVisible() ? 0 : 1 );
			theTransition.setToValue( aPlayer.isVisible() ? 1 : 0 );
			theTransition.setCycleCount( 1 );
			theTransition.setAutoReverse( false );
			theTransition.play();
		}
	}

	//	private void fxPlayerVisibility( P aPlayer, boolean isVisible, int aDurationMillis ) {
	//		Node theSprite;
	//		synchronized ( _playerToSprite ) {
	//			theSprite = _playerToSprite.get( aPlayer );
	//		}
	//		if ( theSprite != null ) {
	//			FadeTransition theTransition = new FadeTransition( Duration.millis( aDurationMillis ), theSprite );
	//			theTransition.setFromValue( isVisible ? 0 : 1 );
	//			theTransition.setToValue( isVisible ? 1 : 0 );
	//			theTransition.setCycleCount( 1 );
	//			theTransition.setAutoReverse( false );
	//			if ( !isVisible ) {
	//				theTransition.setOnFinished( new EventHandler<ActionEvent>() {
	//					@Override
	//					public void handle( ActionEvent event ) {
	//						theSprite.setVisible( false );
	//					}
	//				} );
	//			}
	//			if ( isVisible ) {
	//				if ( !theSprite.visibleProperty().getValue() ) theSprite.setVisible( true );
	//			}
	//			theTransition.play();
	//		}
	//	}

	private void fxRemovePlayer( P aPlayer, int aDurationMillis ) {
		final Node theSprite = removeSprite( aPlayer );
		// Mouse events |-->
		final DragPlayerEventHandler theDragEventHandler = _playerToDragEventHandler.remove( aPlayer );
		if ( theDragEventHandler != null ) {
			theDragEventHandler.dispose();
		}
		final ClickPlayerEventHandler theClickEventHandler = _playerToClickEventHandler.remove( aPlayer );
		if ( theClickEventHandler != null ) {
			theClickEventHandler.dispose();
		}
		// Mouse events <--|
		if ( theSprite != null ) {
			final FadeTransition theTransition = new FadeTransition( Duration.millis( aDurationMillis ), theSprite );
			theTransition.setFromValue( 1 );
			theTransition.setToValue( 0 );
			theTransition.setCycleCount( 1 );
			theTransition.setAutoReverse( false );
			theTransition.setOnFinished( ( ActionEvent event ) -> {
				if ( Platform.isFxApplicationThread() ) {
					_checkers.getChildren().remove( theSprite );
				}
				else {
					Platform.runLater( () -> _checkers.getChildren().remove( theSprite ) );
				}
			} );
			theTransition.play();
		}
	}

	private void fxResetPlayer( P aPlayer, int aDurationMillis ) {
		final Node theSprite;
		final Node thePrevSprite;
		synchronized ( _playerToSprite ) {
			theSprite = createSprite( aPlayer.getStatus() );
			thePrevSprite = putSprite( aPlayer, theSprite );
		}
		if ( aPlayer.isDraggable() ) {
			final DragPlayerEventHandler theDragEventHandler = new DragPlayerEventHandler( aPlayer, theSprite );
			_playerToDragEventHandler.put( aPlayer, theDragEventHandler );
		}
		final ClickPlayerEventHandler theClickEventHandler = new ClickPlayerEventHandler( aPlayer, theSprite );
		_playerToClickEventHandler.put( aPlayer, theClickEventHandler );
		theSprite.setOpacity( 0 );
		final Point2D thePoint = theSprite.localToParent( 0, 0 );
		theSprite.setLayoutX( -( thePoint.getX() ) );
		theSprite.setLayoutY( -( thePoint.getY() ) );
		theSprite.setTranslateX( toPixelPositionX( aPlayer ) );
		theSprite.setTranslateY( toPixelPositionY( aPlayer ) );
		_checkers.getChildren().add( theSprite );
		if ( thePrevSprite != null ) {
			_checkers.getChildren().remove( thePrevSprite );
		}
		final Runnable theRunner = () -> {
			if ( aPlayer.isVisible() && theSprite.getOpacity() == 0 ) {
				final FadeTransition theTransition = new FadeTransition( Duration.millis( aDurationMillis ), theSprite );
				theTransition.setFromValue( 0 );
				theTransition.setToValue( 1 );
				theTransition.setCycleCount( 1 );
				theTransition.setAutoReverse( false );
				theTransition.play();
			}
		};
		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	private void fxScalePlayer( P aPlayer, FieldDimension aFieldDimension ) {
		final Node theSprite = getSprite( aPlayer );
		theSprite.setScaleX( aFieldDimension.getFieldWidth() / theSprite.getBoundsInLocal().getWidth() );
		theSprite.setScaleY( aFieldDimension.getFieldHeight() / theSprite.getBoundsInLocal().getHeight() );
		final double theLayoutX = ( aFieldDimension.getFieldWidth() - theSprite.getBoundsInLocal().getWidth() ) / 2;
		final double theLayoutY = ( aFieldDimension.getFieldHeight() - theSprite.getBoundsInLocal().getHeight() ) / 2;
		theSprite.setLayoutX( theLayoutX );
		theSprite.setLayoutY( theLayoutY );
		theSprite.setTranslateX( toPixelPositionX( aPlayer ) );
		theSprite.setTranslateY( toPixelPositionY( aPlayer ) );
	}

	private Node putSprite( P aPlayer, final Node theSprite ) {
		synchronized ( _playerToSprite ) {
			return _playerToSprite.put( aPlayer, theSprite );
		}
	}

	private Node getSprite( P aPlayer ) {
		synchronized ( _playerToSprite ) {
			return _playerToSprite.get( aPlayer );
		}
	}

	private Node removeSprite( P aPlayer ) {
		synchronized ( _playerToSprite ) {
			return _playerToSprite.remove( aPlayer );
		}
	}

	private boolean isViewport( Position aPosition ) {
		return isViewportPosX( aPosition.getPositionX() ) && isViewportPosY( aPosition.getPositionY() );
	}

	private boolean isViewportPosX( int aPositionX ) {
		return ( aPositionX >= getViewportOffsetX() && aPositionX < getViewportWidth() + getViewportOffsetX() );
	}

	private boolean isViewportPosY( int aPositionY ) {
		return ( aPositionY >= getViewportOffsetY() && aPositionY < getViewportHeight() + getViewportOffsetY() );
	}

	private int toPixelPositionX( Position aPosition ) {
		return ( getFieldWidth() + getFieldGap() ) * aPosition.getPositionX() + ( getGridMode() == GridMode.CLOSED ? getFieldGap() : 0 );
	}

	private int toPixelPositionY( Position aPosition ) {
		return ( getFieldHeight() + getFieldGap() ) * aPosition.getPositionY() + ( getGridMode() == GridMode.CLOSED ? getFieldGap() : 0 );
	}

	private Node createSprite( S theStatus ) {
		final Node theSprite = _spriteFactory.create( theStatus, this );
		theSprite.setUserData( System.identityHashCode( theSprite ) );
		return theSprite;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class ClickPlayerEventHandler implements Disposable {

		private final EventHandler<MouseEvent> _onMouseClickedEventHandler = new EventHandler<MouseEvent>() {
			@Override
			public void handle( MouseEvent aEvent ) {
				if ( _player != null && _player.isVisible() ) {
					LOGGER.log( Level.FINE, "Player mouse press X := " + aEvent.getSceneX() );
					LOGGER.log( Level.FINE, "Player mouse press Y := " + aEvent.getSceneY() );
					_player.click();
					aEvent.consume();
				}
			}
		};
		private P _player;
		private Node _sprite;

		/**
		 * Instantiates a new drag player event handler.
		 *
		 * @param aPlayer the player
		 * @param aSprite the sprite
		 */
		public ClickPlayerEventHandler( P aPlayer, Node aSprite ) {
			aSprite.setOnMouseClicked( _onMouseClickedEventHandler );
			_player = aPlayer;
			_sprite = aSprite;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void dispose() {
			_sprite.removeEventHandler( MouseEvent.MOUSE_CLICKED, _onMouseClickedEventHandler );
			_sprite = null;
			_player = null;
		}
	}

	private class DragPlayerEventHandler implements Disposable {

		private P _player;
		private double _sceneX;
		private double _sceneY;
		private Node _sprite;
		private double _translateX;
		private double _translateY;

		private final EventHandler<MouseEvent> _onMouseDraggedEventHandler = ( MouseEvent aEvent ) -> {
			if ( _player.isVisible() ) {
				final Node theSprite = (Node) ( aEvent.getSource() );
				final double theSceneOffsetX = aEvent.getSceneX() - _sceneX;
				final double theSceneOffsetY = aEvent.getSceneY() - _sceneY;
				final double theTranslateX = _translateX + theSceneOffsetX;
				final double theTranslateY = _translateY + theSceneOffsetY;
				// if ( theTranslateX < -(_bordersH / 2) ) return;
				// if ( theTranslateY < -(_bordersV / 2) ) return;
				final int theBorderWidth = (int) _bordersH / 2;
				final int theBorderHeight = (int) _bordersV / 2;
				// @formatter:off
				/*
					 LOGGER.log( Level.FINE, "Translate X := " + theTranslateX );
					 LOGGER.log( Level.FINE, "Translate Y := " + theTranslateY );
					 LOGGER.log( Level.FINE, "Field width = " + getFieldWidth() );
					 LOGGER.log( Level.FINE, "Field height = " + getFieldHeight() );
					 LOGGER.log( Level.FINE, "Field gap = " + getFieldGap() );
					 LOGGER.log( Level.FINE, "Viewport offset X = " + getViewportOffsetX() );
					 LOGGER.log( Level.FINE, "Viewport offset Y = " + getViewportOffsetY() );
					 LOGGER.log( Level.FINE, "Grid width = " + getGridWidth() );
					 LOGGER.log( Level.FINE, "Grid height = " + getGridHeight() );
					 LOGGER.log( Level.FINE, "Border width := " + theBorderWidth );
					 LOGGER.log( Level.FINE, "Border height := " + theBorderHeight );
					 LOGGER.log( Level.FINE, "Absolute Field width := " + (getFieldWidth() + getFieldGap()) );
					 LOGGER.log( Level.FINE, "Absolute Field height := " + (getFieldHeight() + getFieldGap()) );
					 LOGGER.log( Level.FINE, "Offset X + grid with - 1 := " + (getViewportOffsetX() + getGridWidth() - 1) );
					 LOGGER.log( Level.FINE, "Offset Y + grid height - 1 := " + (getViewportOffsetY() + getGridHeight() - 1) );
					 LOGGER.log( Level.FINE, "... for X := " + ( getFieldWidth() + getFieldGap()) * (getViewportOffsetX() + getGridWidth() - 1) );
					 LOGGER.log( Level.FINE, "... for Y := " + ( getFieldHeight() + getFieldGap()) * (getViewportOffsetY() + getGridHeight() - 1) );
				*/
				// @formatter:on
				if ( theTranslateX >= -theBorderWidth && theTranslateX <= ( getFieldWidth() + getFieldGap() ) * ( getGridWidth() - 1 ) + theBorderWidth ) {
					theSprite.setTranslateX( theTranslateX );
				}
				if ( theTranslateY >= -theBorderHeight && theTranslateY <= ( getFieldHeight() + getFieldGap() ) * ( getGridHeight() - 1 ) + theBorderHeight ) {
					theSprite.setTranslateY( theTranslateY );
				}
				aEvent.consume();
			}
		};

		private final EventHandler<MouseEvent> _onMousePressedEventHandler = ( MouseEvent aEvent ) -> {
			if ( _player.isVisible() ) {
				final Node theSprite = (Node) ( aEvent.getSource() );
				// _checkerboard.getChildren().remove( theSprite );
				// _checkerboard.getChildren().add( theSprite );
				theSprite.setOpacity( 0.5 );
				_sceneX = aEvent.getSceneX();
				_sceneY = aEvent.getSceneY();
				_translateX = theSprite.getTranslateX();
				_translateY = theSprite.getTranslateY();
				LOGGER.log( Level.FINE, "Player mouse press X := " + aEvent.getSceneX() );
				LOGGER.log( Level.FINE, "Player mouse press Y := " + aEvent.getSceneY() );
				aEvent.consume();
			}
		};

		private final EventHandler<MouseEvent> _onMouseReleasedEventHandler = ( MouseEvent aEvent ) -> {
			final Node theSprite = (Node) ( aEvent.getSource() );
			if ( _player.isVisible() ) {
				final double theSceneOffsetX = aEvent.getSceneX() - _sceneX;
				final double theSceneOffsetY = aEvent.getSceneY() - _sceneY;
				double theStepX = Math.round( theSceneOffsetX / ( getFieldWidth() + getFieldGap() ) );
				double theStepY = Math.round( theSceneOffsetY / ( getFieldHeight() + getFieldGap() ) );
				int thePosX = (int) ( _player.getPositionX() + theStepX );
				int thePosY = (int) ( _player.getPositionY() + theStepY );
				if ( thePosX >= getGridWidth() || thePosX < 0 || thePosY >= getGridHeight() || thePosY < 0 ) {
					theStepY = 0;
					theStepX = 0;
					thePosY = (int) ( _player.getPositionY() + theStepY );
					thePosX = (int) ( _player.getPositionX() + theStepX );
				}
				try {
					_player.setPosition( thePosX, thePosY );
				}
				catch ( VetoRuntimeException e ) {
					LOGGER.log( Level.WARNING, "Change player <" + _player + "> position to (" + thePosX + ", " + thePosY + ") has been vetoed as of: " + Trap.asMessage( e ), e );
					theStepX = 0;
					theStepY = 0;
				}
				if ( theStepX == 0 && theStepY == 0 ) {
					fxMovePlayer( _player, _movePlayerDurationMillis );
				}
				theSprite.setOpacity( 1 );
		// @formatter:off
				/*
					LOGGER.log( Level.FINE, "Player mouse release X := " + aEvent.getSceneX() );
					LOGGER.log( Level.FINE, "Player mouse release Y := " + aEvent.getSceneY() );
					LOGGER.log( Level.FINE, "Player mouse release offset X := " + theSceneOffsetX );
					LOGGER.log( Level.FINE, "Player mouse release offset Y := " + theSceneOffsetY );
					LOGGER.log( Level.FINE, "Player grid step X := " + theStepX );
					LOGGER.log( Level.FINE, "Player grid step Y := " + theStepY );
					LOGGER.log( Level.FINE, "Player new pos X := " + thePosX );
					LOGGER.log( Level.FINE, "Player new pos Y := " + thePosY );
				*/
				// @formatter:on
				aEvent.consume();
			}

			// else {
			// theSprite.setTranslateX( toPositionX( _player ) );
			// theSprite.setTranslateY( toPositionY( _player ) );
			// }
		};

		/**
		 * Instantiates a new drag player event handler.
		 *
		 * @param aPlayer the player
		 * @param aSprite the sprite
		 */
		public DragPlayerEventHandler( P aPlayer, Node aSprite ) {
			aSprite.setOnMousePressed( _onMousePressedEventHandler );
			aSprite.setOnMouseDragged( _onMouseDraggedEventHandler );
			aSprite.setOnMouseReleased( _onMouseReleasedEventHandler );
			_player = aPlayer;
			_sprite = aSprite;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void dispose() {
			_sprite.removeEventHandler( MouseEvent.MOUSE_PRESSED, _onMousePressedEventHandler );
			_sprite.removeEventHandler( MouseEvent.MOUSE_DRAGGED, _onMouseDraggedEventHandler );
			_sprite.removeEventHandler( MouseEvent.MOUSE_RELEASED, _onMouseReleasedEventHandler );
			_sprite = null;
			_player = null;
		}
	}
}