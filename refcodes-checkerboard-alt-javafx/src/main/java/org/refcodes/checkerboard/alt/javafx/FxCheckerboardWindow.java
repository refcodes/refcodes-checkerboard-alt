package org.refcodes.checkerboard.alt.javafx;

import java.util.logging.Level;

import org.refcodes.checkerboard.Checkerboard;
import org.refcodes.checkerboard.CheckerboardViewer;
import org.refcodes.checkerboard.GridDimensionChangedEvent;
import org.refcodes.checkerboard.Player;
import org.refcodes.checkerboard.ViewportDimensionChangedEvent;
import org.refcodes.graphical.GridDimension;
import org.refcodes.graphical.ViewportDimension;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * The class {@link FxCheckerboardWindow} uses the {@link FxCheckerboardViewer}
 * to implement a resizable(!) {@link CheckerboardViewer}.
 * 
 * For scaling, this might be an idea:
 * "http://gillius.org/blog/2013/02/javafx-window-scaling-on-resize.html"
 *
 * @param <P> The type representing a {@link Player}
 * @param <S> The type which's instances represent a {@link Player} state.
 * 
 * @deprecated Deprecated as of being untested and under development
 */
@Deprecated
class FxCheckerboardWindow<P extends Player<P, S>, S> extends FxCheckerboardViewer<P, S> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Stage _stage;
	private final Scene _scene;
	private final int _resizeGridMillis = 500;

	// -------------------------------------------------------------------------
	// WINDOW HEIGHT CHANGE HANDLER:
	// -------------------------------------------------------------------------

	private final ChangeListener<Number> _onWindowHeightChangedEventHandler = new ChangeListener<Number>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void changed( ObservableValue<? extends Number> observable, Number aOldValue, Number aNewValue ) {
			// _lastResizeTimeInMs = System.currentTimeMillis();
			// if ( Math.abs( aNewValue.doubleValue() - aOldValue.doubleValue() ) > 10 ) return;

			// -----------------------------------------------------------------
			// Calculate the sum of the H-borders:
			// -----------------------------------------------------------------
			if ( Double.isNaN( _bordersV ) && !Double.isNaN( aOldValue.doubleValue() ) ) {
				initBordersV( aOldValue.doubleValue() );
				initMinStageHeight();
			}
			// -----------------------------------------------------------------

			switch ( getScaleMode() ) {
			case GRID -> {
				final int theNewViewportHeight = toScaledViewportDimension( aNewValue.doubleValue(), getViewportHeight(), getFieldHeight(), getFieldGap(), _bordersV + _windowDecorationV );
				if ( theNewViewportHeight != -1 ) {
					LOGGER.log( Level.FINE, "Viewport height changed to := " + theNewViewportHeight );
					setViewportHeight( theNewViewportHeight );
				}
			}
			case FIELDS -> {
				final int theNewFieldHeight = toScaledFieldDimension( aNewValue.doubleValue(), getViewportHeight(), getFieldHeight(), getFieldGap(), _bordersV + _windowDecorationV );
				if ( theNewFieldHeight != -1 ) {
					LOGGER.log( Level.FINE, "Field height changed to := " + theNewFieldHeight );
					setFieldHeight( theNewFieldHeight );
				}
			}
			case NONE -> {
			}
			}
		}
	};

	// -------------------------------------------------------------------------
	// WINDOW WIDTH CHANGE HANDLER:
	// -------------------------------------------------------------------------

	private final ChangeListener<Number> _onWindowWidthChangedEventHandler = new ChangeListener<Number>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void changed( ObservableValue<? extends Number> observable, Number aOldValue, Number aNewValue ) {

			// _lastResizeTimeInMs = System.currentTimeMillis();
			// if ( Math.abs( aNewValue.doubleValue() - aOldValue.doubleValue() ) > 10 ) return;

			// -----------------------------------------------------------------
			// Calculate the sum of the H-borders:
			// -----------------------------------------------------------------
			if ( Double.isNaN( _bordersH ) && !Double.isNaN( aOldValue.doubleValue() ) ) {
				initBordersH( aOldValue.doubleValue() );
				initMinStageWidth();
			}
			// -----------------------------------------------------------------

			switch ( getScaleMode() ) {
			case GRID -> {
				final int theNewViewportWidth = toScaledViewportDimension( aNewValue.doubleValue(), getViewportWidth(), getFieldWidth(), getFieldGap(), _bordersH + _windowDecorationH );
				if ( theNewViewportWidth != -1 ) {
					LOGGER.log( Level.FINE, "Viewport width changed to := " + theNewViewportWidth );
					setViewportWidth( theNewViewportWidth );
				}
			}
			case FIELDS -> {
				final int theNewFieldWidth = toScaledFieldDimension( aNewValue.doubleValue(), getViewportWidth(), getFieldWidth(), getFieldGap(), _bordersH + _windowDecorationH );
				if ( theNewFieldWidth != -1 ) {
					LOGGER.log( Level.FINE, "Field width changed to := " + theNewFieldWidth );
					setFieldWidth( theNewFieldWidth );
				}
			}
			case NONE -> {
			}
			}
		}
	};

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link FxCheckerboardWindow} instance. ATTENTION: As
	 * no {@link FxSpriteFactory} is provided to this constructor, no sprites
	 * can be fabricated when players are added until the
	 * {@link #setSpriteFactory(org.refcodes.checkerboard.SpriteFactory)} has
	 * been set!
	 *
	 * @param aStage The {@link Stage} embedding the {@link Scene}.
	 * @param aScene The {@link Scene} embedding the
	 *        {@link FxCheckerboardWindow}.
	 * @param aCheckerboard the {@link Checkerboard} to be viewed.
	 */
	public FxCheckerboardWindow( Stage aStage, Scene aScene, Checkerboard<P, S> aCheckerboard ) {
		super( aCheckerboard );
		widthProperty().addListener( _onWindowWidthChangedEventHandler );
		heightProperty().addListener( _onWindowHeightChangedEventHandler );
		_stage = aStage;
		_scene = aScene;
		_stage.widthProperty().addListener( evt -> fxResizeStage() );
		_stage.heightProperty().addListener( evt -> fxResizeStage() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onGridDimensionChangedEvent( GridDimensionChangedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		fxResizeGrid( aEvent, aEvent.getPrecedingGridDimension(), _resizeGridMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onViewportDimensionChangedEvent( ViewportDimensionChangedEvent<P, S> aEvent ) {
		LOGGER.log( Level.FINE, aEvent.toString() );
		fxResizeViewport( aEvent );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	private synchronized void fxResizeGrid( GridDimension aDimension, GridDimension aPrecedingDimension, int aDurationMillis ) {
		final Runnable theRunner = () -> {
			if ( _backgroundFactory != null ) {
				fxUpdateBackground( aDimension, aPrecedingDimension, aDurationMillis );
			}
		};

		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	private synchronized void fxResizeViewport( ViewportDimension aDimension ) {
		final Runnable theRunner = new Runnable() {
			@Override
			public void run() {
				setFieldDimension( FxCheckerboardWindow.this );
				setViewportDimension( aDimension );
			}
		};

		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	private void fxResizeStage() {
		final Runnable theRunner = () -> {
			if ( _stage != null ) {
				switch ( getScaleMode() ) {
				case GRID, FIELDS -> {
					if ( _stage != null ) {
						_stage.setScene( _scene );
					}
				}
				case NONE -> {
					if ( _stage != null ) {
						_stage.setScene( _scene );
						_stage.sizeToScene();
						_stage.setResizable( false );
					}
				}
				}
			}
		};
		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	//	private synchronized void fxResizeFields( FieldDimensionChangedEvent<S, FxSpriteFactory<S>, FxCheckerboardWindow<P, S>> aEvent, FieldDimension precedingFieldDimension ) {
	//		Runnable theRunner = new Runnable() {
	//			@Override
	//			public void run() {
	//				if ( _backgroundFactory != null ) {
	//					int index = 0;
	//					Node theOldBackgroundNode = _backgroundNode;
	//					_backgroundNode = _backgroundFactory.create( FxCheckerboardWindow.this );
	//					if ( theOldBackgroundNode != null ) {
	//						index = _checkers.getChildren().indexOf( theOldBackgroundNode );
	//						_checkers.getChildren().remove( _checkers.getChildren().remove( theOldBackgroundNode ) );
	//					}
	//					_checkers.getChildren().add( index, _backgroundNode );
	//				}
	//				fxResizeStage();
	//			}
	//		};
	//		if ( Platform.isFxApplicationThread() ) {
	//			theRunner.run();
	//		}
	//		else {
	//			Platform.runLater( theRunner );
	//		}
	//	}

	private void fxUpdateBackground( GridDimension aDimension, GridDimension aPrecedingDimension, int aDurationMillis ) {
		int index = 0;
		final Node theOldBackgroundNode = _backgroundNode;
		final Node theNewBackgroundNode = _backgroundFactory.create( this );
		final FadeTransition theTransition = new FadeTransition( Duration.millis( aDurationMillis ) );
		if ( theOldBackgroundNode != null ) {
			index = _checkers.getChildren().indexOf( theOldBackgroundNode );
			theTransition.setOnFinished( ( ActionEvent event ) -> _checkers.getChildren().remove( theOldBackgroundNode ) );
		}
		Node theTransitionNode = null;
		double theFromValue = 0;
		double theToValue = 1;
		if ( aPrecedingDimension != null && aDimension.getGridWidth() >= aPrecedingDimension.getGridWidth() && aDimension.getGridHeight() >= aPrecedingDimension.getGridHeight() ) {
			theTransitionNode = theNewBackgroundNode;
			theNewBackgroundNode.setOpacity( 0 );
			theFromValue = 0;
			theToValue = 1;
			index++;
		}
		else if ( aPrecedingDimension != null && aDimension.getGridWidth() <= aPrecedingDimension.getGridWidth() && aDimension.getGridHeight() <= aPrecedingDimension.getGridHeight() ) {
			theTransitionNode = theOldBackgroundNode;
			theFromValue = 1;
			theToValue = 0;
		}

		if ( aPrecedingDimension == null ) {
			theTransitionNode = theNewBackgroundNode;
			theNewBackgroundNode.setOpacity( 0 );
		}

		_backgroundNode = theNewBackgroundNode;
		_checkers.getChildren().add( index, _backgroundNode );
		theTransition.setNode( theTransitionNode );
		theTransition.setFromValue( theFromValue );
		theTransition.setToValue( theToValue );
		theTransition.setCycleCount( 1 );
		theTransition.setAutoReverse( false );
		theTransition.play();
	}
}