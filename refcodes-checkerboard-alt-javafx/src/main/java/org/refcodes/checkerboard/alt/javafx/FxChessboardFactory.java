// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard.alt.javafx;

import org.refcodes.graphical.Raster;
import org.refcodes.graphical.ext.javafx.FxRasterFactory;

import javafx.scene.Node;
import javafx.scene.paint.Color;

/**
 * A {@link FxBackgroundFactory} creating checkerboard {@link Node} backgrounds.
 */
public class FxChessboardFactory implements FxBackgroundFactory {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final FxRasterFactory _factory = new FxRasterFactory();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder method for the even field color.
	 *
	 * @param aEvenFieldColor the a even field color
	 * 
	 * @return the FxChessboardFactory as of the builder pattern.
	 */
	public FxChessboardFactory withEvenFieldColor( Color aEvenFieldColor ) {
		_factory.setEvenFieldColor( aEvenFieldColor );
		return this;
	}

	/**
	 * Builder method for the odd field color.
	 *
	 * @param aOddFieldColor the a odd field color
	 * 
	 * @return the FxChessboardFactory as of the builder pattern.
	 */
	public FxChessboardFactory withOddFieldColor( Color aOddFieldColor ) {
		_factory.setOddFieldColor( aOddFieldColor );
		return this;
	}

	/**
	 * Builder method for the field gap color.
	 *
	 * @param aFieldGapColor the a field gap color
	 * 
	 * @return the FxChessboardFactory as of the builder pattern.
	 */
	public FxChessboardFactory withFieldGapColor( Color aFieldGapColor ) {
		_factory.setFieldGapColor( aFieldGapColor );
		return this;
	}

	/**
	 * Gets the even field color.
	 *
	 * @return the even field color
	 */
	public Color getEvenFieldColor() {
		return _factory.getEvenFieldColor();
	}

	/**
	 * Sets the even field color.
	 *
	 * @param eEvenFieldColor the new even field color
	 */
	public void setEvenFieldColor( Color eEvenFieldColor ) {
		_factory.setEvenFieldColor( eEvenFieldColor );
	}

	/**
	 * Gets the odd field color.
	 *
	 * @return the odd field color
	 */
	public Color getOddFieldColor() {
		return _factory.getOddFieldColor();
	}

	/**
	 * Sets the odd field color.
	 *
	 * @param aOddFieldColor the new odd field color
	 */
	public void setOddFieldColor( Color aOddFieldColor ) {
		_factory.setOddFieldColor( aOddFieldColor );
	}

	/**
	 * Sets the field gap color.
	 *
	 * @param aFieldGapColor the new field gap color
	 */
	public void setFieldGapColor( Color aFieldGapColor ) {
		_factory.setFieldGapColor( aFieldGapColor );
	}

	/**
	 * Gets the field gap color.
	 *
	 * @return the field gap color
	 */
	public Color getFieldGapColor() {
		return _factory.getFieldGapColor();
	}

	// /////////////////////////////////////////////////////////////////////////
	// FACTORIES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Node create( Raster aContext ) {
		return _factory.create( aContext );
	}
}
