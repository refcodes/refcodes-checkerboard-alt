// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard.alt.javafx;

import java.util.Map;
import java.util.WeakHashMap;

import org.refcodes.checkerboard.SpriteFactory;

import javafx.scene.Node;

/**
 * A factory for creating sprites for the {@link FxCheckerboardViewer}.
 *
 * @param <S> the generic type of the identifier for which the sprite is to be
 *        created.
 */
public interface FxSpriteFactory<S> extends SpriteFactory<Node, S, FxCheckerboardViewer<?, S>> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Some words when using the {@link FxCheckerboardViewer} implementation of
	 * the {@link FxCheckerboardViewer}: In case you return a {@link Node} for
	 * the provided identifier being the same instance as a previously created
	 * {@link Node} for the same previously provided identifier, then the
	 * {@link Node} is just redrawn by the {@link FxCheckerboardViewer}
	 * (preventing fading out / fading in when updating the {@link Node}). If
	 * another instance is returned, then the previously set {@link Node} is
	 * removed (fade out) before the newly created {@link Node} is added (fade
	 * in) by the {@link FxCheckerboardViewer}. You may use a {@link Map}
	 * ({@link WeakHashMap}) for relating the identifier to the according
	 * {@link Node} in order to identify whether to create a new {@link Node} or
	 * update an existing one. {@inheritDoc}
	 */
	@Override
	Node create( S aIdentifier, FxCheckerboardViewer<?, S> aContext );
}
